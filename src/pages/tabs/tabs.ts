import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
// import { SupportPage } from '../support/support';
import { ConsultantsPage } from "../consultants/consultants";
import { PackagesPage } from '../packages/packages';
import { ConsultationPage } from '../consultation/consultation';
import { AccountPage } from '../account/account';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = AccountPage;
  tab2Root = ConsultationPage;
  tab3Root = PackagesPage;
  tab4Root = ConsultantsPage;

  mySelectedIndex: number;
  isConsultant = false;

  constructor(navParams: NavParams, public storage: Storage) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
    this.storage.get('userData')
      .then((userData) => {
        if (userData.TypeId == "3") {
          this.isConsultant = true;
        }
      });
  }

}
