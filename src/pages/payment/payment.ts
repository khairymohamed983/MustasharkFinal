import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { Stripe } from '@ionic-native/stripe';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { NgForm } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
  card: any = {
    number: '',  // 4242424242424242
    expMonth: '', // 12
    expYear: '', // 2020
    cvc: '' //220
  };
  fullDate: string = '';
  package: any = {};
  constructor(public navCtrl: NavController,
    public storage: Storage,
    public http: Http,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public stripe: Stripe) {
    this.package = navParams.data;
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }
  makePayment(paymentForm: NgForm) {
    if (paymentForm.valid) {
      this.stripe.setPublishableKey('pk_test_rEwDsKUUmJ43xagDnB1XEfEJ');
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: '<div class="loader"></div>'
      });
      loading.present();
      let cc = this.fullDate.split('-');
      console.log(cc);
      this.card.expMonth = cc[1];
      this.card.expYear = cc[0];
      this.stripe.createCardToken(this.card)
        .then(token => {
          let model = {
            PackageId: this.package.Id,
            StripeTokenId: token.id,
            Amount: this.package.Price
          };
          this.storage.get('userData')
            .then((userData) => {
              if (userData) {
                let headers = new Headers({ 'Content-Type': 'application/json' });
                headers.append('Authorization', 'Bearer ' + userData.access_token);
                let options = new RequestOptions({ headers: headers });
                this.http.post('http://viable.cbcloudapps.com/api/Packages/RegisterInPackage', model, options)
                  .subscribe(res => {
                    loading.dismiss();
                    let alert = this.alertCtrl.create({
                      title: 'الاشتراك في باقة ',
                      subTitle: res.json().message,
                      buttons: ['تم']
                    });
                    alert.present();
                    this.navCtrl.push("TabsPage");
                  }, (err) => {
                    loading.dismiss();
                    let alert = this.alertCtrl.create({
                      title: 'حدث خطأ ما ',
                      subTitle: 'نعتذر ولكن يبدو أن هناك خطأ ما ' + err.status,
                      buttons: ['تم']
                    });
                    alert.present();
                  });
              }
            });
        })
        .catch(error => {
          alert(error);
          loading.dismiss();
        });
    } else {
      let alert = this.alertCtrl.create({
        title: 'قم بإكمال بيانات الدفع ',
        subTitle: 'يرجي اكمال بيانات الدفع أولا ',
        buttons: ['تم']
      });
      alert.present();
    }
  }
}
