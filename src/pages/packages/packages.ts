import { Component, ViewChild } from '@angular/core';
import { NavController, Refresher, Events, LoadingController, Slides, AlertController } from 'ionic-angular';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { InAppPurchase } from '@ionic-native/in-app-purchase';

@Component({
    selector: 'page-packages',
    templateUrl: 'packages.html'
})

export class PackagesPage {
    packages: any[] = [];
    @ViewChild(Slides) slides: Slides;
    isConsultant = false;
    loaded = false;
    constructor(
        public navCtrl: NavController, public http: Http,
        public events: Events, public loadingCtrl: LoadingController,
        public alert: AlertController,
        private iap: InAppPurchase,
        public storage: Storage) {
        this.storage.get('userData')
            .then((userData) => {
                if (userData.TypeId == "3") {
                    this.isConsultant = true;
                }
            });
    }

    ionViewWillEnter() {
    }

    subscribe(pack: any) {
        let model = {
            PackageId: pack.Id
        };

        this.storage.get('userData')
            .then((userData) => {
                let loading = this.loadingCtrl.create({
                    spinner: 'hide',
                    content: '<div class="loader"></div>'
                });

                debugger;
                loading.present();

                let headers = new Headers({ 'Content-Type': 'application/json' });
                headers.append('Authorization', 'Bearer ' + userData.access_token);
                let options = new RequestOptions({ headers: headers });

                this.http.get('http://viable.cbcloudapps.com/api/Packages/CheckBeforeSubscribe', options).subscribe(res => {
                    // console.log(res.json())
                    var rs = res.json();
                    if (rs.code == 0) { 
                        if (pack.Price == 0) {
                            this.http.post('http://viable.cbcloudapps.com/api/Packages/RegisterInPackage', model, options)
                                .subscribe(res => {
                                    loading.dismiss();
                                    let alert = this.alert.create({
                                        title: 'تمت عملية الاشتراك ',
                                        subTitle: 'انت الآن مشترك في باقة ' + pack.Title,
                                        buttons: ['تم']
                                    });
                                    alert.present();
                                    this.navCtrl.push("TabsPage");
                                }, (err) => {
                                    loading.dismiss();
                                    let alert = this.alert.create({
                                        title: 'حدث خطأ ما ',
                                        subTitle: 'نعتذر ولكن يبدو أن هناك خطأ ما ' + err.status,
                                        buttons: ['تم']
                                    });
                                    alert.present();
                                });
                        } else {
                            // alert(pack.StoreId);
                            // in-app purchase logic goinng here 
                            // this.navCtrl.push('PaymentPage', pack);
                            // let productId = pack.StoreId.replace("com.octohub.mustashark.", "");

                            this.iap.subscribe(pack.StoreId).then((res) => {
                                alert(res.transactionId)
                                this.http.post('http://viable.cbcloudapps.com/api/Packages/RegisterInPackage', model, options)
                                    .subscribe(res1 => {
                                        loading.dismiss();
                                        let alert = this.alert.create({
                                            title: 'تمت عملية الاشتراك ',
                                            subTitle: 'انت الآن مشترك في باقة ' + pack.Title,
                                            buttons: ['تم']
                                        });
                                        alert.present();
                                        this.navCtrl.push("TabsPage");
                                    }, (err) => {
                                        loading.dismiss();
                                        let alert = this.alert.create({
                                            title: 'حدث خطأ ما ',
                                            subTitle: 'نعتذر ولكن يبدو أن هناك خطأ ما ' + err.status,
                                            buttons: ['تم']
                                        });
                                        alert.present();
                                    });
                            }).catch((err) => {
                                loading.dismiss();
                                alert(JSON.stringify(err));
                            });

                        }
                    } else {
                        loading.dismiss();
                        let alert = this.alert.create({
                            title: 'حدث خطأ ما ',
                            subTitle: rs.message,
                            buttons: ['تم']
                        });
                        alert.present();
                    }
                }, (err) => {
                    loading.dismiss();
                    let alert = this.alert.create({
                        title: 'حدث خطأ ما ',
                        subTitle: 'لم نتمكن من تآكيد اشتراكك الحالي الرجاء المحاوله في وقت لاحق  ',
                        buttons: ['تم']
                    });
                    alert.present();
                });
            });
    }

    gonext() {
        if (!this.slides.isEnd()) {
            this.slides.slideNext();
        }
    }

    gopreviuse() {
        if (!this.slides.isBeginning()) {
            this.slides.slidePrev();
        }
    }

    ionViewDidLoad() {
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: '<div class="loader"></div>'
        });
        loading.present();
        this.http.get('http://viable.cbcloudapps.com/api/packages/1')
            .subscribe(res => {
                loading.dismiss();
                this.packages = res.json();
                console.log(this.packages);
                this.loaded = true;
                this.slides.update();
            }, (err) => {
                loading.dismiss();
                this.loaded = true;
            });
    }

    doRefresh(refresher: Refresher) {
        this.loaded = false;
        this.http.get('http://viable.cbcloudapps.com/api/packages/1')
            .subscribe(res => {
                refresher.complete();
                this.packages = res.json();
                this.slides.update();
                this.loaded = true;

            }, (err) => {
                this.loaded = true;
                refresher.complete();
            });
    }
}
