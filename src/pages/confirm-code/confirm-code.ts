import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController } from 'ionic-angular';
import { NgForm } from '@angular/forms';

import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';


@IonicPage()
@Component({
  selector: 'page-confirm-code',
  templateUrl: 'confirm-code.html',
})
export class ConfirmCodePage {

  data: any = {
    UserName: '',
    ActivationCode: ''
  }
  password: string
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http,
    public events: Events, public loadingCtrl: LoadingController,
    public storage: Storage) {
    this.data.UserName = this.navParams.data.Email;
    this.password = this.navParams.data.Password
  }

  onLogin(form: NgForm) {
    if (form.valid) {
        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({ headers: headers }); // Create a request option
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: '<div class="loader"></div>'
        });
        loading.present();
        this.http.post('http://viable.cbcloudapps.com/api/Account/Activate', JSON.stringify(this.data), options)
            .subscribe(res => {
                console.log(JSON.stringify(res.json()));
                loading.dismiss();
                let loading2 = this.loadingCtrl.create({
                    spinner: 'hide',
                    content: '<div class="loader"></div>'
                });
                loading2.present();
                let headers2 = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
                let options2 = new RequestOptions({ headers: headers2 });
                this.http.get('http://viable.cbcloudapps.com/api/authorize/get?' + "username=" + this.data.UserName + "&password=" + this.password, options2)
                    .subscribe(res => {
                        let result = res.json();
                        this.storage.set('hasLoggedIn', true);
                        this.storage.set('username', result.username);
                        this.storage.set('userData', result);
                        this.events.publish('user:login');
                        loading2.dismiss();
                    }, (err) => {
                        console.log(err);
                        loading2.dismiss();
                    });
            }, (err) => {
                console.log(err);
                loading.dismiss();
            });
    }
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmCodePage');
  }

}
