import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsultationChatPage } from './consultation-chat';

@NgModule({
  declarations: [
    ConsultationChatPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsultationChatPage),
  ],entryComponents:[
    ConsultationChatPage
  ]
})
export class ConsultationChatPageModule {}
