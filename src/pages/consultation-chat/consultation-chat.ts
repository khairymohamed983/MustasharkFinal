import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, NavController, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { Events, Content, TextInput } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';
import { hubConnection } from 'signalr-no-jquery';
import { Camera } from '@ionic-native/camera';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
const connection = hubConnection('http://viable.cbcloudapps.com/signalr/');
const hubProxy = connection.createHubProxy('chatHub');

@IonicPage()
@Component({
  selector: 'page-consultation-chat',
  templateUrl: 'consultation-chat.html',
})
export class ConsultationChatPage {
  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: TextInput;

  msgList: any[] = [];
  consultationId: string;
  me: any = {
    email: '',
    userName: '',
    image: ''
  }
  other: any = {
    email: '',
    userName: '',
    image: ''
  }
  
  connected = false;
  consultationName: string;
  editorMsg: string = '';
  isMessagePushed: boolean = false;
  isConsultant: boolean = false;
  consultation: any;
  loaded = false;
  isClosed = false;

  constructor(public navParams: NavParams,
    public storage: Storage,
    public events: Events,
    private toast: ToastController,
    public http: Http,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private camera: Camera,
    private base64ToGallery: Base64ToGallery
  ) {
    this.consultationName = navParams.data.Title;
    this.consultationId = navParams.data.Id;
    this.consultation = navParams.data;
    console.log(navParams.data);
  }

  resize() {

    if (!this.messageInput._native.nativeElement.value) {
      this.messageInput._native.nativeElement.style.height = "auto";
    } else {
      this.messageInput._native.nativeElement.style.height = this.messageInput._native.nativeElement.scrollHeight + 'px';
    }
  }
  myTrim(x: string) {
    return x.replace(/\r?\n|\r/g, '').replace(/^\s+|\s+$/gm, '');
  }

  ionViewDidLoad() {
    this.storage.get('userData')
      .then((userData: any) => {
        if (userData) {
          if (userData.TypeId == "3") {
            this.isConsultant = true;
          }
          this.me = {
            email: userData.Email,
            userName: userData.Name,
            image: userData.ImageUrl
          }
          const id = userData.userName;
          const consultaionIdd = this.consultationId;
          let _th = this;
          hubProxy.on('recieveMessage', function (fromUserEmail: string, message: string, isImage: boolean, date: string, time: string) {
            _th.editorMsg = '';
            console.log('a message were sent : ' + message);
            if (!_th.isMessagePushed || fromUserEmail != _th.me.email) {
              _th.pushNewMsg(fromUserEmail, message, isImage, date, time);
              _th.isMessagePushed = true;
            }
          });
          connection.start()
            .done(function () {
              console.log('Now connected, connection ID=' + connection.id);
              hubProxy.invoke('connect', id, consultaionIdd).then((msg) => {
                console.log(msg);
                _th.connected = true;
              }, (reason) => {
                console.log(reason);
              });
            }).fail(function () { alert('Could not Connect to Mustashark Chating Server '); });

          let headers = new Headers({ 'Content-Type': 'application/json' });
          headers.append('Authorization', 'Bearer ' + userData.access_token);

          let options = new RequestOptions({ headers: headers });
          let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: '<div class="loader"></div>'
          });

          let encodedUrl = 'http://viable.cbcloudapps.com/api/Consultations/GetAllReplies?ConsultationId=' + this.consultationId;
          loading.present();

          this.http.get('http://viable.cbcloudapps.com/api/account/getuserchatdata?consultationid=' + this.consultationId, options).subscribe(res => {
            const oth = res.json()[0];
            console.log(oth);
            this.other = {
              email: oth.Email,
              userName: oth.FullName,
              image: oth.Image
            }
            this.http.get(encodedUrl, options)
              .subscribe(res => {
                this.msgList = res.json();
                loading.dismiss();
                this.loaded = true;

                if (this.content._scroll) this.scrollToBottom();
              }, (err: any) => {
                this.loaded = true;
                loading.dismiss();
                if (err.status == 401) {
                  let alert = this.alertCtrl.create({
                    title: 'حدث خطأ ما ',
                    subTitle: 'يرجي تسجيل الخروج وتسجيل الدخول من جديد ',
                    buttons: ['تم']
                  });
                  alert.present();
                } else if (err.status == 400) {
                  let alert = this.alertCtrl.create({
                    title: 'حدث خطأ ما ',
                    subTitle: 'نعتذر ولكن يبدو أن هناك خطأ ما ',
                    buttons: ['تم']
                  });
                  alert.present();
                } else {
                  let alert = this.alertCtrl.create({
                    title: 'حدث خطأ ما ',
                    subTitle: 'نعتذر ولكن يبدو أن هناك خطأ ما ' + err.status,
                    buttons: ['تم']
                  });
                  alert.present();
                }
              });
          }, err => {
            let alert = this.alertCtrl.create({
              title: 'حدث خطأ ما ',
              subTitle: 'نعتذر ولكن يبدو أن هناك خطأ ما ' + err.status,
              buttons: ['تم']
            });
            alert.present();
          });

        }
      });
    if (this.content) this.scrollToBottom();
  }

  pick() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      quality: 40,
      encodingType: this.camera.EncodingType.JPEG,
    }).then((imageData: any) => {
      try {
        this.send("data:image/jpeg;base64," + imageData, true);
      } catch (err) {
        console.log(err);
      }
    }, (err: any) => {
      console.log(err);
    });
  }

  browse() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      quality: 40,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((imageData: any) => {
      try {
        this.send("data:image/jpeg;base64," + imageData, true);
      } catch (err) {
        console.log(err);
      }
    }, (err: any) => {
      console.log(err);
    });
  }

  send(content: string, isImage: boolean) {
    this.disabled = true;
    this.isMessagePushed = false;
    this.storage.get('userData')
      .then((userData) => {
        if (userData) {
          let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
          headers.append('Authorization', 'Bearer ' + userData.access_token);
          let options = new RequestOptions({ headers: headers }); // Create a request option
          this.http.post('http://viable.cbcloudapps.com/api/Consultations/Reply', { ConsultationId: this.consultationId, message: content, isImage: isImage }, options)
            .subscribe(res => {
              console.log(res);
            }, (err) => {
              alert(err.code);
            });
        } else {
        }
      });
  }

  sendMessage() {
    if (this.editorMsg) {
      this.send(this.editorMsg, false);
    }
  }

  _focus() {
    this.content.resize();
    if (this.content._scroll) this.scrollToBottom();
  }

  disabled: boolean = false;

  getImage(userEmail) {
    if (userEmail === this.me.email) {
      return this.me.image;
    } else {
      return this.other.image;
    }
  }
  getUserName(userEmail) {
    if (userEmail === this.me.email) {
      return this.me.userName;
    } else {
      return this.other.userName;
    }
  }
  public pushNewMsg(fromUser: any, msg: string, isImage: boolean, creationDate: string, creationTime: string) {
    if (this.disabled) {
      this.disabled = false;
    }
    let newMsg = {
      FromUserEmail: fromUser,
      Message: msg,
      // UserName: userName,
      // UserImage: getImage(),
      strDate: creationDate,
      strTime: creationTime,
      IsImage: isImage
    };
    this.msgList.push(newMsg);
    if (this.content._scroll) this.scrollToBottom();
  }

  closeConultation() {
    this.storage.get('userData')
      .then((userData) => {
        if (userData) {
          console.log(userData);
          let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
          headers.append('Authorization', 'Bearer ' + userData.access_token);
          let options = new RequestOptions({ headers: headers }); // Create a request option
          let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: '<div class="loader"></div>'
          });
          loading.present();
          this.http.post('http://viable.cbcloudapps.com/api/Consultations/close/', { id: this.consultationId }, options)
            .subscribe(res => {
              console.log(res);
              loading.dismiss();
              this.navCtrl.setRoot('ConsultationPage');
            }, (err) => {
              alert(err.code);
              loading.dismiss();
            });
        } else {
        }
      });
  }

  download(base64Data) {
    console.log(base64Data);
    this.base64ToGallery.base64ToGallery(base64Data.split(',')[1], { prefix: 'mustashark_' }).then(
      res => {

        let toast = this.toast.create({
          message: 'قد تم حفظ الصورة في مجلد الصور الخاصة بك ',
          duration: 6000
        });
        toast.present();
      },
      err => {
        let toast = this.toast.create({
          message: 'لم نتمكن من حفظ الصورة يبدو انك غير متصل ',
          duration: 6000
        });
        toast.present();
      }
    );
  }


  openDetails() {
    if (this.isConsultant) {
      this.viewUserDetails();
    } else {
      this.viewConsultantProfile();
    }
  }

  viewConsultantProfile() {
    this.navCtrl.push('ConsultantDetailsPage', {
      ClosedConsultationCount: this.consultation.ConsultantClosedConsultationCount,
      Email: this.consultation.ConsultantEmail,
      FullName: this.consultation.ConsultantFullName,
      Image: this.consultation.ConsultantImage,
      OpenConsultationCount: this.consultation.ConsultantOpenConsultationCount,
      Summary: this.consultation.ConsultantSummary
    });
  }
  viewUserDetails() {
    if (this.consultation.UserTypeId == 4) { // client
      this.navCtrl.push('ClientDetailsPage', {
        Image: this.consultation.UserImage,
        FullName: this.consultation.UserFullName,
        Mobile: this.consultation.UserMobile,
        BirthDate: this.consultation.UserDateOfBirth,
        Gender: this.consultation.Gender == 1 ? "ذكر" : "أنثي",
        Field: this.consultation.Field,
        SalesVolumn: this.consultation.SalesVolum,
        Capital: this.consultation.Capital,
        NoOfEmps: this.consultation.NoOfEmps,
        NoOfYears: this.consultation.NoOfYears,
        Website: this.consultation.Website,
        HavePlane: this.consultation.HavePlane == true ? "نعم" : "كلا",
        HaveSystem: this.consultation.HaveSystem == true ? "نعم" : "كلا"
      });
    } else {
      this.navCtrl.push('EmployeeDetailsPage', {
        Image: this.consultation.UserImage,
        FullName: this.consultation.UserFullName,
        Mobile: this.consultation.UserMobile,
        BirthDate: this.consultation.UserDateOfBirth,
        Gender: this.consultation.Gender == 1 ? "ذكر" : "أنثي",
        IsSupervisor: this.consultation.IsSupervisor == true ? "نعم" : "كلا",
        LastJobTitle1: this.consultation.LastJobTitle1,
        LastJobTitle2: this.consultation.LastJobTitle2,
        LastJobTitle3: this.consultation.LastJobTitle3,
        Employer: this.consultation.Employer,
        CurrentJobTitle: this.consultation.CurrentJobTitle,
        LearningLevel: this.consultation.LearningLevel,
        ExperianceYears: this.consultation.ExperianceYears
      });
    }
  }

  gotoConsultationDetails() {
    this.navCtrl.push('ConsultationDetailPage', this.consultation);
  }

  scrollToBottom() {
    try {
      setTimeout(() => {
        if (this.content.scrollToBottom) {
          this.content.scrollToBottom();
        }
      }, 400)
    } catch (x) {
    }
  }
}
