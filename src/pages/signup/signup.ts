import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { IonicPage, NavController, Events, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  signup = {
    Name: '',
    Email: '',
    Password: '',
    ConfirmPassword: '',
    TypeId: 1
  };

  submitted = false;
  constructor(public navCtrl: NavController, public http: Http,
    public events: Events, public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    public storage: Storage) { }
  showPass1 = false;
  showPass2 = false;
  type1: string = 'password';
  type2: string = 'password';
  showPassword(i: number) {
    switch (i) {
      case 1:
        this.showPass1 = !this.showPass1;
        if (this.showPass1) {
          this.type1 = 'text';
        } else {
          this.type1 = 'password';
        }
        break;
      case 2:
        this.showPass2 = !this.showPass2;
        if (this.showPass2) {
          this.type2 = 'text';
        } else {
          this.type2 = 'password';
        }
        break;
    }
  }

  goToLogin() {
    this.navCtrl.push('LoginPage');
  }

  onSignup(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
      let options = new RequestOptions({ headers: headers }); // Create a request option
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: '<div class="loader"></div>'
      });
      console.log(this.signup);
      
      loading.present();
      this.http.post('http://viable.cbcloudapps.com/api/Account/Register', JSON.stringify(this.signup), options)
        .subscribe(res => {
          console.log(res.json());
          loading.dismiss();
          this.navCtrl.push('ConfirmCodePage', this.signup);
        }, (err) => {
          var er = JSON.parse(err._body);
          let ale = this.alertCtrl.create({
            title: 'هناك خطأ في البيانات',
            subTitle: er.Message,
            buttons: ['تم']
          });
          ale.present();
          loading.dismiss();
        });
    }
  }

}
