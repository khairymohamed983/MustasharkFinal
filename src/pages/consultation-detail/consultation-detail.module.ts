import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsultationDetailPage } from './consultation-detail';

@NgModule({
  declarations: [
    ConsultationDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsultationDetailPage),
  ],entryComponents:[
    ConsultationDetailPage
  ]
})
export class ConsultationDetailPageModule {}
