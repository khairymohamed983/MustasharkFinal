import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';



@IonicPage()
@Component({
    selector: 'page-consultation-detail',
    templateUrl: 'consultation-detail.html',
})
export class ConsultationDetailPage {

    consultation: any = {};
    isConsultant: boolean = false;

    constructor(
        public navParams: NavParams,
        public navCtrl: NavController,
        public http: Http,
        public loadingCtrl: LoadingController,
        public storage: Storage
    ) {
        this.consultation = this.navParams.data;
    }

    ionViewWillEnter() {
        this.consultation = this.navParams.data;
        this.storage.get('userData').then((userData) => {
            if (userData) {
                if (userData.TypeId == "3") {
                    this.isConsultant = true;
                }
            }
        });
    }
    review() {
        this.navCtrl.push('ConsultationChatPage');
    }

    viewUserDetails() {
        if (this.consultation.UserTypeId == 4) { // client
            this.navCtrl.push('ClientDetailsPage', {
                Image: this.consultation.UserImage,
                FullName: this.consultation.UserFullName,
                Mobile: this.consultation.UserMobile,
                BirthDate: this.consultation.UserDateOfBirth,
                Gender: this.consultation.Gender == 1 ? "ذكر" : "أنثي",
                Field: this.consultation.Field,
                SalesVolumn: this.consultation.SalesVolum,
                Capital: this.consultation.Capital,
                NoOfEmps: this.consultation.NoOfEmps,
                NoOfYears: this.consultation.NoOfYears,
                Website: this.consultation.Website,
                HavePlane: this.consultation.HavePlane == true ? "نعم" : "كلا",
                HaveSystem: this.consultation.HaveSystem == true ? "نعم" : "كلا"
            });
        } else { // employee
            this.navCtrl.push('EmployeeDetailsPage', {
                Image: this.consultation.UserImage,
                FullName: this.consultation.UserFullName,
                Mobile: this.consultation.UserMobile,
                BirthDate: this.consultation.UserDateOfBirth,
                Gender: this.consultation.Gender == 1 ? "ذكر" : "أنثي",
                IsSupervisor: this.consultation.IsSupervisor == true ? "نعم" : "كلا",
                LastJobTitle1: this.consultation.LastJobTitle1,
                LastJobTitle2: this.consultation.LastJobTitle2,
                LastJobTitle3: this.consultation.LastJobTitle3,
                Employer: this.consultation.Employer,
                CurrentJobTitle: this.consultation.CurrentJobTitle,
                LearningLevel: this.consultation.LearningLevel,
                ExperianceYears: this.consultation.ExperianceYears
            });
        }
    }

    viewConsultantProfile() {
        this.navCtrl.push('ConsultantDetailsPage', {
            ClosedConsultationCount: this.consultation.ConsultantClosedConsultationCount,
            Email: this.consultation.ConsultantEmail,
            FullName: this.consultation.ConsultantFullName,
            Image: this.consultation.ConsultantImage,
            OpenConsultationCount: this.consultation.ConsultantOpenConsultationCount,
            Summary: this.consultation.ConsultantSummary
        });
    }

    accept() {
        this.storage.get('userData')
            .then((userData) => {
                if (userData) {
                    console.log(userData);
                    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
                    headers.append('Authorization', 'Bearer ' + userData.access_token);
                    let options = new RequestOptions({ headers: headers }); // Create a request option
                    let loading = this.loadingCtrl.create({
                        spinner: 'hide',
                        content: '<div class="loader"></div>'
                    });
                    loading.present();
                    this.http.post('http://viable.cbcloudapps.com/api/Consultations/Accept', { id: this.consultation.Id }, options)
                        .subscribe(res => {
                            console.log("ٍsucess : " + res.json().message);
                            loading.dismiss();
                            this.navCtrl.push('ConsultationChatPage', res);
                        }, (err) => {
                            console.log("Error ???!!" + JSON.stringify(err));
                            loading.dismiss();
                        });
                } else {
                }
            });
    }

    refuse() {
        this.storage.get('userData')
            .then((userData) => {
                if (userData) {
                    console.log(userData);
                    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
                    headers.append('Authorization', 'Bearer ' + userData.access_token);
                    let options = new RequestOptions({ headers: headers }); // Create a request option
                    let loading = this.loadingCtrl.create({
                        spinner: 'hide',
                        content: '<div class="loader"></div>'
                    });
                    loading.present();
                    this.http.post('http://viable.cbcloudapps.com/api/Consultations/Cancel/', { id: this.consultation.Id }, options)
                        .subscribe(res => {
                            alert('Success ')
                            alert(res.json().message);
                            loading.dismiss();
                            this.navCtrl.push('ConsultationPage');
                        }, (err) => {
                            alert(err.code);
                            loading.dismiss();
                        });
                }
            });
    }


}
