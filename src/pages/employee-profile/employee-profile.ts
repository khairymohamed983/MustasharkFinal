import { Component } from '@angular/core';
import { AlertController,LoadingController,IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';


@IonicPage()
@Component({
  selector: 'page-employee-profile',
  templateUrl: 'employee-profile.html',
})
export class EmployeeProfilePage {
  model: any = {
    Name: '',
    Mobile: '',
    BirthDate: '',
    Gender: 1,
    Employer: '',
    LastJobTitle1: '',
    LastJobTitle2: '',
    LastJobTitle3: '',
    CurrentJobTitle: '',
    IsSupervisor: true,
    ExperianceYears: 1,
    LearningLevel: ''
}

constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public storage: Storage,
    public camera: Camera) { }

ngAfterViewInit() {
    this.storage.get('userData')
        .then((userData) => {
            console.log(userData);
            this.model.Name = userData.Name;
            this.model.Mobile = userData.Mobile;
            var arr = userData.DateOfBirth.split('/');
            this.model.BirthDate = arr[2] + '-' + arr[1] + '-' + arr[0];// "2002-12-19"
            this.model.Gender = userData.Gender;
            this.model.Employer = userData.Employer;
            this.model.LastJobTitle1 = userData.LastJobTitle1;
            this.model.LastJobTitle2 = userData.LastJobTitle2;
            this.model.LastJobTitle3 = userData.LastJobTitle3;
            this.model.CurrentJobTitle = userData.CurrentJobTitle;
            this.model.IsSupervisor = new Boolean(userData.IsSupervisor).toString();
            this.model.ExperianceYears = userData.ExperianceYears;
            this.model.LearningLevel = userData.LearningLevel;
        });
}

ionViewDidLoad() {
    console.log('ionViewDidLoad EmployeeProfilePage');
}

saveChanges(form: any) {
    if (form.valid) {
        this.storage.get('userData')
            .then((userData) => {
                if (userData) {
                    console.log(this.model);
                    var arr = this.model.BirthDate.split('-');
                    this.model.BirthDate = arr[2] + '/' + arr[1] + '/' + arr[0];                        // "2002-12-19"
                    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
                    headers.append('Authorization', 'Bearer ' + userData.access_token);
                    let options = new RequestOptions({ headers: headers }); // Create a request option
                    let loading = this.loadingCtrl.create({
                        spinner: 'hide',
                        content: '<div class="loader"></div>'
                    });
                    loading.present();
                    this.http.post('http://viable.cbcloudapps.com/api/Account/UpdateEmployeeProfile', this.model, options)
                        .subscribe(res => {
                            console.log(res.json());
                            loading.dismiss();
                            this.storage.get('userData')
                                .then((userData) => {
                                    userData.Name = this.model.Name;
                                    this.model.Mobile = userData.Mobile;
                                    userData.DateOfBirth = this.model.BirthDate
                                    userData.Gender = this.model.Gender;
                                    userData.Employer = this.model.Employer;
                                    userData.LastJobTitle1 = this.model.LastJobTitle1;
                                    userData.LastJobTitle2 = this.model.LastJobTitle2;
                                    userData.LastJobTitle3 = this.model.LastJobTitle3;
                                    userData.CurrentJobTitle = this.model.CurrentJobTitle;
                                    userData.IsSupervisor = this.model.IsSupervisor;
                                    userData.ExperianceYears = this.model.ExperianceYears;
                                    userData.LearningLevel = this.model.LearningLevel;
                                    this.storage.set('userData', userData).then((ss) => {
                                        this.navCtrl.setRoot('AccountPage');
                                    }, (err) => {
                                        alert(err);
                                    });
                                });
                        }, (err) => {
                            alert(err);
                            loading.dismiss();
                        });
                } else {
                }
            });
    }
}

}
