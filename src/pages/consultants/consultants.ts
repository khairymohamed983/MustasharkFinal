import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Refresher } from 'ionic-angular';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-consultants',
  templateUrl: 'consultants.html',
})
export class ConsultantsPage {
  consultants = [];
  loaded = false;

  constructor(public navCtrl: NavController, public navParams: NavParams
    , public loadingCtrl: LoadingController
    , public http: Http, public storage: Storage) { }

  ionViewDidLoad() {
    this.storage.get('userData')
      .then((userData) => {
        if (userData) {
          let headers = new Headers({ 'Content-Type': 'application/json' });
          headers.append('Authorization', 'Bearer ' + userData.access_token);
          let options = new RequestOptions({ headers: headers });
          let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: '<div class="loader"></div>'
          });
          let encodedUrl = 'http://viable.cbcloudapps.com/api/account/GetAllConsultant';
          loading.present();
          this.http.get(encodedUrl, options)
            .subscribe(res => {
              loading.dismiss();
              this.consultants = this.processArray(res.json(), 2);
              this.loaded = true;
            }, (err) => {
              loading.dismiss();
              this.loaded = true;
            });
        }
      });
  }

  doRefresh(refresher: Refresher) {
    this.loaded = false;
    this.storage.get('userData')
      .then((userData) => {
        if (userData) {
          let headers = new Headers({ 'Content-Type': 'application/json' });
          headers.append('Authorization', 'Bearer ' + userData.access_token);
          let options = new RequestOptions({ headers: headers });
          let encodedUrl = 'http://viable.cbcloudapps.com/api/account/GetAllConsultant';
          this.http.get(encodedUrl, options)
            .subscribe(res => {
              refresher.complete();
              this.consultants = this.processArray(res.json(), 2);
              this.loaded = true;
            }, (err) => {
              this.loaded = true;
              refresher.complete();
            });
        }
      });
  }

  processArray(serverData, chunkSize) {
    var groups = [], i;
    for (i = 0; i < serverData.length; i += chunkSize) {
      groups.push(serverData.slice(i, i + chunkSize));
    }
    return groups;
  }

  openDetails(consultant: any) {
    this.navCtrl.push('ConsultantDetailsPage', consultant);
  }
}
