import { Component } from '@angular/core';
import { IonicPage,LoadingController, NavController, Events, ToastController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';
import { NgForm } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  login = { username: '', password: '' };
  submitted = false;
  constructor(public navCtrl: NavController, public http: Http,
      public events: Events, public loadingCtrl: LoadingController,
      public storage: Storage,
      public toastCtrl: ToastController, public alertCtrl: AlertController) { }

  showPass1 = false;
  type1: string = 'password';
  showPassword(i: number) {
      switch (i) {
          case 1:
              this.showPass1 = !this.showPass1;
              if (this.showPass1) {
                  this.type1 = 'text';
              } else {
                  this.type1 = 'password';
              }
              break;
      }
  }
  onLogin(form: NgForm) {
    this.submitted = true;
    if (form.valid) {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        let loading = this.loadingCtrl.create({
            spinner: 'hide',                        
            content: '<div class="loader"></div>'});
        // let body = "grant_type=password&
        loading.present();
        this.http.get('http://viable.cbcloudapps.com/api/authorize/get?' + "username=" + this.login.username + "&password=" + this.login.password, options)
            .subscribe(res => {
                let result = res.json(); 
                this.storage.set('hasLoggedIn', true);
                this.storage.set('username', result.username);
                this.storage.set('userData', result);
                this.events.publish('user:login');
                // this.navCtrl.setRoot(TabsPage);
                loading.dismissAll(); 
            }, (err) => {
                loading.dismiss();                    
                try {
                    var errArr = err._body.split(':')[1].split(',');
                    var code = errArr[0].split('=')[1];
                    var message = errArr[1].split('=')[1]
                    message = message.substring(0, message.length - 3);
                    if (code == -2) {
                        let toast = this.toastCtrl.create({
                            message: message,
                            duration: 6000
                        });
                        toast.present();
                        this.navCtrl.push('ConfirmCodePage', { Email: this.login.username, Password: this.login.password });
                    } else if (code == -1) {// mail or password is invalid
                        let alert = this.alertCtrl.create({
                            title: 'بيانات الدخول خاطئة',
                            subTitle: message,
                            buttons: ['تم']
                        });
                        alert.present();
                    } else if (code == -3) { 
                        let alert = this.alertCtrl.create({
                            title: 'حسابك غير مفعل',
                            subTitle: message,
                            buttons: ['تم']
                        });
                        alert.present();
                    } else {
                        let alert = this.alertCtrl.create({
                            title: 'حدث خطأ ما ',
                            subTitle: message,
                            buttons: ['تم']
                        });
                        alert.present();
                    }
                } catch (er) {
                    let alert = this.alertCtrl.create({
                        title: 'حدث خطأ في الدخول ',
                        subTitle: "لم نتمكن من تسجيل الدخول ",
                        buttons: ['تم']
                    });
                    alert.present();
                }
            });
    } else {

    }
}

onSignup() {
    this.navCtrl.push('SignupPage');
}
onForgetPassword() {
    this.navCtrl.push('ForgetPasswordPage');        
}

}
