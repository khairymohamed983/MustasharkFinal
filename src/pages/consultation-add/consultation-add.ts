import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';

import { Camera } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';
import { ConsultationPage } from '../consultation/consultation';

@IonicPage()
@Component({
  selector: 'page-consultation-add',
  templateUrl: 'consultation-add.html',
})
export class ConsultationAddPage {
  @ViewChild('myInput') myInput;
  ordinaryHeight = "auto";
  consultationsRest: any;
  consultationsRestDueDate: any;
  consultation: any = {
    Title: "",
    Details: "",
    Image: null
  }
  resize() {
    if (!this.myInput._native.nativeElement.value) {
      this.myInput._native.nativeElement.style.height = this.ordinaryHeight;
    } else {
      this.myInput._native.nativeElement.style.height = this.myInput._native.nativeElement.scrollHeight + 'px';
    }
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController,
    public http: Http,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    private camera: Camera) {
    this.consultationsRest = 5;
    this.consultationsRestDueDate = "12/9/2017";
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsultationAddPage');
  }
  dismiss(data?: any) {
    this.viewCtrl.dismiss(data);
  }
  pick() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      //targetWidth: 1000,
      //targetHeight: 1000,
      quality: 40,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((imageData: any) => {
      try {
        this.consultation.Image = "data:image/jpeg;base64," + imageData;
      } catch (err) {
        alert(err);
      }
    }, (err: any) => {
      alert(err);
      console.log(err);
    });
  }
  browse() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      quality: 40,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((imageData: any) => {
      try {
        this.consultation.Image = "data:image/jpeg;base64," + imageData;
      } catch (err) {
        alert(err);
      }
    }, (err: any) => {
      alert(err);
      console.log(err);
    });
  }

  save(loginForm: any) {
    if (loginForm.valid) {
      this.storage.get('userData')
        .then((userData) => {
          if (userData) {
            let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
            headers.append('Authorization', 'Bearer ' + userData.access_token);
            let options = new RequestOptions({ headers: headers }); // Create a request option
            let loading = this.loadingCtrl.create({
              spinner: 'hide',
              content: '<div class="loader"></div>'
            });
            loading.present();
            this.http.post('http://viable.cbcloudapps.com/api/Consultations/Add', this.consultation, options)
              .subscribe(res => {
                loading.dismiss(); 
                this.navCtrl.setRoot(ConsultationPage);
              }, (err) => {
                alert(err.message);
                loading.dismiss();
              });
          } else {

          }
        });
    } else {
      let alert = this.alertCtrl.create({
        title: 'الرجاء إكمال ملأ بيانات الاستشارة ',
        subTitle: 'نعتذر ولكن يجب ان تكمل بيانات الاستشارة اولا  ',
        buttons: ['تم']
      });
      alert.present();
    }
  }
}