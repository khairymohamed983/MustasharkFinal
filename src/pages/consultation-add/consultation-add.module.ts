import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsultationAddPage } from './consultation-add';

@NgModule({
  declarations: [
    ConsultationAddPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsultationAddPage),
  ],
  entryComponents:[
    ConsultationAddPage
  ]
})
export class ConsultationAddPageModule {}
