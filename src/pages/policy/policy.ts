import { Component } from '@angular/core';
import { IonicPage ,LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';
 

@IonicPage()
@Component({
  selector: 'page-policy',
  templateUrl: 'policy.html',
})
export class PolicyPage {

  constructor(public http: Http, public storage: Storage, public loadingCtrl: LoadingController) { }
  policyContent = '';
  ionViewDidLoad() {

    this.storage.get('userData')
      .then((userData) => {
        if (userData) {
          console.log(userData);
          let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
          headers.append('Authorization', 'Bearer ' + userData.access_token);
          let options = new RequestOptions({ headers: headers }); // Create a request option
          let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: '<div class="loader"></div>'
          });
          loading.present();
          //api/Content/UsePrivilage
          this.http.get('http://viable.cbcloudapps.com/api/Content/UsePrivilage', options)
            .subscribe(res => {
              console.log(res);
              this.policyContent = res.json();
              loading.dismiss();
            }, (err) => {
              alert(err.code);
              loading.dismiss();
            });
        } else {
        }
      });
  }

}
