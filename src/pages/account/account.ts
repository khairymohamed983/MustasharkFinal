import { Component } from '@angular/core';
import {   NavController, ModalController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
 import { Http, RequestOptions, Headers } from '@angular/http';
import { PackagesPage } from '../packages/packages';

@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
  username: string;
  imageUrl: string;
  currentPackage: string;
  RestQuestions: string;
  AskedQuestions: string;
  CurrentPackageEndDate: string;
  userType: string;
  accountType: string;
  isConsultant = false;
  consultantDetails = '';
  constructor(public alertCtrl: AlertController,public http : Http ,
    public nav: NavController, public modalCtrl: ModalController, public storage: Storage) {
 
    }

  ionViewWillEnter() {
    this.storage.get('next').then((next) => {
      if (next) {
        this.nav.push(next).then(() => {
          this.storage.set('next', undefined);
        });
      }
    });
    this.getUsername();
  }

  public navigate(pagename) {
    this.nav.push(pagename);
  }

  ionViewDidLoad() {
  }

  gotoPackages(){
    this.nav.setRoot(PackagesPage);    
  }

  presentAdd() {
    this.nav.push('ConsultationAddPage');
  }

  navigateToConsultations() {
    this.nav.push('ConsultationPage');
  }
  
  gotoChangePic() {
    this.nav.push('ChangeProfilePicPage');
  }

  updateProfile() {
    switch (this.userType) {
      case "3":
        this.nav.push("ConsltantProfilePage");
        break;
      case "4":
        this.nav.push("ClientProfilePage");
        break;
      case "5":
        this.nav.push("EmployeeProfilePage");
        break;
      default:
        this.nav.push("EmployeeProfilePage");
        break;
    }
  }

  changePassword() {
    this.nav.push('ChangePasswordPage');
  }

  getUsername() {
    this.storage.get('userData')
      .then((userData) => {
        console.log(userData);
        if (userData.TypeId == "3") {
          this.isConsultant = true;
          this.consultantDetails = userData.Qulifications
        }
        this.username = userData.Name;
        this.imageUrl = userData.ImageUrl;

        this.currentPackage = userData.CurrentPackage;
        this.RestQuestions =  userData.RestQuestions;
        this.AskedQuestions = userData.AskedQuestions;
        this.userType = userData.TypeId;

        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        headers.append('Authorization', 'Bearer ' + userData.access_token);
        let options = new RequestOptions({ headers: headers }); // Create a request option
        this.http.get('http://viable.cbcloudapps.com/api/Account/RefreshData', options).subscribe(res => {
          var dat = res.json();
          userData.CurrentPackage = dat.CurrentPackage;
          userData.RestQuestions = dat.RestQuestions;
          userData.AskedQuestions = dat.AskedQuestions;
          userData.CurrentPackageEndDate = dat.CurrentPackageEndDate;
          this.currentPackage = userData.CurrentPackage;
          this.RestQuestions =  userData.RestQuestions;
          this.AskedQuestions = userData.AskedQuestions;   
          this.storage.set('userData',userData);
        }, (err) => {
          console.log(err);
        });

        this.CurrentPackageEndDate = userData.CurrentPackageEndDate;
        switch (this.userType) {
          case "3":
            // this.nav.push("ConsltantProfilePage");
            this.accountType = "مستشار";
            break;
          case "4":
            this.accountType = "صاحب عمل";
            break;
          case "5":
            this.accountType = "موظف";
            break;
          default:
            this.accountType = "موظف";
            break;
        }
      });
  }

}
