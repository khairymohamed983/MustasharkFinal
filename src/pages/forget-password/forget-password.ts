import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController, AlertController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';
 

@IonicPage()
@Component({
  selector: 'page-forget-password',
  templateUrl: 'forget-password.html',
})
export class ForgetPasswordPage {

  login = { username: '' };
  cur: number = 1;
  data: any = {
    UserName: '',
    ActivationCode: ''
  }

  model: any = {
    NewPassword: '',
    ConfirmPassword: ''
  }
  type2: string = 'password';
  type3: string = 'password';
  showPass2 = false;
  showPass3 = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, private alertCtrl: AlertController) {
  }

  showPassword(i: number) {
    switch (i) {
      case 2:
        this.showPass2 = !this.showPass2;
        if (this.showPass2) {
          this.type2 = 'text';
        } else {
          this.type2 = 'password';
        }
        break;
      case 3:
        this.showPass3 = !this.showPass3;
        if (this.showPass3) {
          this.type3 = 'text';
        } else {
          this.type3 = 'password';
        }
        break;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetPasswordPage');
  }
  backtoLogin() {
    this.navCtrl.setRoot('LoginPage');
  }
  onLogin(form: NgForm) {
    if (form.valid) {
      this.resend();
    } else {
    }
  }
  resend() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: '<div class="loader"></div>'
    });
    loading.present();
    this.http.post('http://viable.cbcloudapps.com/api/Account/ForgetPassword/', { UserName: this.login.username })
      .subscribe(res => {
        let result = res.json();
        loading.dismissAll();
        let alert = this.alertCtrl.create({
          title: 'تم ارسال الكود',
          subTitle: result.Message,
          buttons: ['تم']
        });
        alert.present();
        this.cur = 2;
      }, (err) => {
        loading.dismiss();

        let alert = this.alertCtrl.create({
          title: 'حدث خطأ ما ',
          subTitle: err.Message,
          buttons: ['تم']
        });
        alert.present();
      });
  }
  validateCode(form: NgForm) {
    if (form.valid) {
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: '<div class="loader"></div>'
      });
      loading.present();
      this.http.post('http://viable.cbcloudapps.com/api/Account/CheckResetPasswordActivationCode/',
        { ActivationCode: this.data.ActivationCode })
        .subscribe(res => {
          let result = res.json();
          let alert = this.alertCtrl.create({
            title: 'تم تأكيد الكود',
            subTitle: result.Message,
            buttons: ['تم']
          });
          alert.present();
          this.cur = 3;
          loading.dismissAll();
        }, (err) => {
          loading.dismiss();

          let alert = this.alertCtrl.create({
            title: 'حدث خطأ ما ',
            subTitle: err.Message,
            buttons: ['تم']
          });
          alert.present();
        });
    } else {
    }
  }

  changePassword(form: NgForm) {
    if (form.valid) {
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: '<div class="loader"></div>'
      });
      loading.present();
      console.log(this.login.username);
      this.http.post('http://viable.cbcloudapps.com/api/Account/RestPassword/',
        {
          UserName: this.login.username,
          NewPassword: this.model.NewPassword
        })
        .subscribe(res => {
          let result = res.json();
          loading.dismissAll();
          var alert = this.alertCtrl.create({ title: result.Message, message: 'سيتم الآن تحويلك إلي صفحة تسجيل الدخول' });
          alert.present();
          this.navCtrl.setRoot('LoginPage');
          alert.dismiss();
        }, (err) => {
          loading.dismiss();
          let alert = this.alertCtrl.create({
            title: 'حدث خطأ ما ',
            subTitle: err.Message,
            buttons: ['تم']
          });
          alert.present();
        });
    } else {
    }
  }


}
