import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeProfilePicPage } from './change-profile-pic';

@NgModule({
  declarations: [
    ChangeProfilePicPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeProfilePicPage),
  ],
  entryComponents:[
    ChangeProfilePicPage
  ]
})
export class ChangeProfilePicPageModule {}
