import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { Http, RequestOptions, Headers } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-change-profile-pic',
  templateUrl: 'change-profile-pic.html',
})
export class ChangeProfilePicPage {
  Image: string = '';
  UserName: string = '';

  constructor(
    private camera: Camera,
    public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public http: Http, public loadingCtrl: LoadingController) {
  }
  ionViewDidLoad() {
    this.storage.get('userData')
      .then((userData) => {
        console.log(userData);
        this.UserName = userData.Email;
        this.Image = userData.ImageUrl;
      });
  }
  pick() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      //targetWidth: 1000,
      //targetHeight: 1000,
      quality: 40,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((imageData: any) => {
      try {
        this.Image = "data:image/jpeg;base64," + imageData;
      } catch (err) {
        alert(err);
      }
    }, (err: any) => {
      alert(err);
      console.log(err);
    });
  }
  save() {
    this.storage.get('userData')
      .then((userData) => {
        if (userData) {
          let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
          headers.append('Authorization', 'Bearer ' + userData.access_token);
          let options = new RequestOptions({ headers: headers }); // Create a request option
          let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: '<div class="loader"></div>'
          });
          loading.present();
          this.http.post('http://viable.cbcloudapps.com/api/Account/UpdateImage', { Image: this.Image }, options)
            .subscribe(res => {
              console.log(res.json());
              loading.dismiss();
              userData.ImageUrl = this.Image;
              this.storage.set('userData', userData)
                .then(() => {
                  this.navCtrl.push('AccountPage');
                });
            }, (err) => {
              alert(err);
              loading.dismiss();
            });
        } else {
        }
      });
  }
  browse() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      quality: 40,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((imageData: any) => {
      try {
        this.Image = "data:image/jpeg;base64," + imageData;
      } catch (err) {
        alert(err);
      }
    }, (err: any) => {
      alert(err);
      console.log(err);
    });
  }
}
