import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsltantProfilePage } from './consltant-profile';

@NgModule({
  declarations: [
    ConsltantProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ConsltantProfilePage),
  ],
  entryComponents:[
    ConsltantProfilePage
  ]
})
export class ConsltantProfilePageModule {}
