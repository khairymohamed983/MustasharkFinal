import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';


@IonicPage()
@Component({
  selector: 'page-consltant-profile',
  templateUrl: 'consltant-profile.html',
})
export class ConsltantProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public camera: Camera) {
    this.storage.get('userData')
      .then((userData) => {
        this.model.Name = userData.Name;
        this.model.Mobile = userData.Mobile;
        this.model.Gender = userData.Gender;
        this.model.Qulifications = userData.Qulifications;
        var arr = userData.DateOfBirth.split('/');
        this.model.BirthDate = arr[2] + '-' + arr[1] + '-' + arr[0];// "2002-12-19"
      });
  }

  model: any = {
    Name: "",
    Mobile: "",
    BirthDate: "",
    Gender: 1,
    Qulifications: ""
  }

  pick() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      quality: 40,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((imageData: any) => {
      try {
        this.model.Image = "data:image/jpeg;base64," + imageData;
      } catch (err) {
        alert(err);
      }
    }, (err: any) => {
      alert(err);
      console.log(err);
    });
  }

  browse() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      quality: 40,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((imageData: any) => {
      try {
        alert(imageData.length);
        this.model.Image = "data:image/jpeg;base64," + imageData;
      } catch (err) {
        alert(err);
      }
    }, (err: any) => {
      alert(err);
      console.log(err);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsltantProfilePage');
  }

  saveChanges(form: any) {
    if (form.valid) {
      this.storage.get('userData')
        .then((userData) => {
          if (userData) {
            console.log(userData);
            var arr = this.model.BirthDate.split('-');
            this.model.BirthDate = arr[2] + '/' + arr[1] + '/' + arr[0];// "2002-12-19"
            let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
            headers.append('Authorization', 'Bearer ' + userData.access_token);
            let options = new RequestOptions({ headers: headers }); // Create a request option
            let loading = this.loadingCtrl.create({
              spinner: 'hide',
              content: '<div class="loader"></div>'
            });
            loading.present();
            console.log(this.model);

            this.http.post('http://viable.cbcloudapps.com/api/Account/UpdateConsultantProfile', this.model, options)
              .subscribe(res => {
                console.log(res.json());
                loading.dismiss();
                userData.Name = this.model.Name;
                userData.Mobile = this.model.Mobile;
                userData.Gender = this.model.Gender;
                userData.Qulifications = this.model.Qulifications;
                userData.DateOfBirth = this.model.BirthDate;
                this.storage.set('userData', userData).then((ss) => {
                  this.navCtrl.setRoot('AccountPage');
                }, (err) => {
                  alert(err);
                });
              }, (err) => {
                alert(err);
                loading.dismiss();
              });
          } else {
          }
        });
    }
  }
}
