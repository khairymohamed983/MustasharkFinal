import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentCompletePage } from './payment-complete';

@NgModule({
  declarations: [
    PaymentCompletePage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentCompletePage),
  ],entryComponents:[
    PaymentCompletePage
  ]
})
export class PaymentCompletePageModule {}
