import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PackageHistoryPage } from './package-history';

@NgModule({
  declarations: [
    PackageHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(PackageHistoryPage),
  ],
  entryComponents:[
    PackageHistoryPage
  ]
})
export class PackageHistoryPageModule {}
