import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-package-history',
  templateUrl: 'package-history.html',
})
export class PackageHistoryPage {
  
  constructor(public navCtrl: NavController,public storage: Storage, public navParams: NavParams , public loadingCtrl: LoadingController, public http: Http) {
  }
  isConsultant = false;
  history :any=[];
  ionViewDidLoad() {
    this.storage.get('userData')
    .then((userData) => {
        if (userData) {
            if (userData.TypeId == "3") {
                this.isConsultant = true;
                return;
            }
            let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
            headers.append('Authorization', 'Bearer ' + userData.access_token);
            let options = new RequestOptions({ headers: headers }); // Create a request option
            let loading = this.loadingCtrl.create({
                spinner: 'hide',                        
                content: '<div class="loader"></div>'            });
            loading.present();
            this.http.get('http://viable.cbcloudapps.com/api/account/userpackages', options)
                .subscribe(res => {
                    // this.history = res.json();
                    console.log(this.history);
                    loading.dismiss();
                }, (err) => {
                    loading.dismiss();
                    alert(err);
                });
        } else {
        }
    });
  }
  gotoPackages(){
      this.navCtrl.push('PackagesPage');
  }


}
