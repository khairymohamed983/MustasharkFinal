import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';


@IonicPage()
@Component({
  selector: 'page-client-profile',
  templateUrl: 'client-profile.html',
})
export class ClientProfilePage {
  model: any = {
    Name: '',
    Image: '',
    Mobile: '',
    BirthDate: '',
    Gender: 1,
    Field: "",
    SalesVolumn: "",
    Capital: 1,
    NoOfEmps: 1,
    NoOfYears: 1,
    Website: "",
    HaveSystem: true,
    HavePlane: true
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public camera: Camera) {
  }

  ngAfterViewInit() {
    this.storage.get('userData')
      .then((userData) => {
        this.model.Name = userData.Name;
        this.model.Mobile = userData.Mobile;
        var arr = userData.DateOfBirth.split('/');
        this.model.BirthDate = arr[2] + '-' + arr[1] + '-' + arr[0];// "2002-12-19"
        this.model.Gender = userData.Gender;
        this.model.Field = userData.Field;
        this.model.SalesVolumn = userData.SalesVolum;
        this.model.Capital = userData.Capital;
        this.model.NoOfEmps = userData.NoOfEmps;
        this.model.NoOfYears = userData.NoOfYears;
        this.model.Website = userData.Website;
        this.model.HaveSystem = userData.HaveSystem;
        this.model.HavePlane = userData.HavePlane;
      });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientProfilePage');
  }
  pick() {
        this.camera.getPicture({
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.CAMERA,
            quality: 40,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        }).then((imageData: any) => {
            try {
                this.model.Image = "data:image/jpeg;base64," + imageData;
            } catch (err) {
                alert(err);
            }
        }, (err: any) => {
            alert(err);
            console.log(err);
        });
    }

    browse() {
        this.camera.getPicture({
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            quality: 40,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        }).then((imageData: any) => {
            try {
                alert(imageData.length);
                this.model.Image = "data:image/jpeg;base64," + imageData;
            } catch (err) {
                alert(err);
            }
        }, (err: any) => {
            alert(err);
            console.log(err);
        });
    }

    saveChanges(form: any) {
        if (form.valid) {
            this.storage.get('userData')
                .then((userData) => {
                    if (userData) {
                        var arr = this.model.BirthDate.split('-');
                        this.model.BirthDate = arr[2] + '/' + arr[1] + '/' + arr[0];                        // "2002-12-19"
                        let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
                        headers.append('Authorization', 'Bearer ' + userData.access_token);
                        let options = new RequestOptions({ headers: headers }); // Create a request option
                        let loading = this.loadingCtrl.create({
                            spinner: 'hide',                        
                            content: '<div class="loader"></div>'                        });
                        loading.present();
                        this.http.post('http://viable.cbcloudapps.com/api/Account/UpdateStackholderProfile', this.model, options)
                            .subscribe(res => {
                                console.log(res.json());
                                 userData.Name= this.model.Name;
                                 userData.Mobile=this.model.Mobile ;
                                 userData.DateOfBirth=this.model.BirthDate;
                                 userData.Gender=this.model.Gender ;
                                 userData.Field=this.model.Field ;
                                 userData.SalesVolum=this.model.SalesVolumn ;
                                 userData.Capital=this.model.Capital ;
                                 userData.NoOfEmps=this.model.NoOfEmps ;
                                 userData.NoOfYears= this.model.NoOfYears;
                                 userData.Website=this.model.Website ;
                                 userData.HaveSystem=this.model.HaveSystem ;
                                 userData.HavePlane=this.model.HavePlane ;
                                 this.storage.set('userData', userData).then((ss) => {
                                    this.navCtrl.setRoot('AccountPage');
                                }, (err) => {
                                    alert(err);
                                });
                                loading.dismiss();
                            }, (err) => {
                                alert(err);
                                loading.dismiss();
                            });
                    } else {
                    }
                });
        }
    }
}
