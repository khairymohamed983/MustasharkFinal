import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsultantDetailsPage } from './consultant-details';

@NgModule({
  declarations: [
    ConsultantDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsultantDetailsPage),
  ],
  entryComponents:[
    ConsultantDetailsPage
  ]
})
export class ConsultantDetailsPageModule {}
