import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

 
@IonicPage()
@Component({
  selector: 'page-consultant-details',
  templateUrl: 'consultant-details.html',
})
export class ConsultantDetailsPage {

  consultant :any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.consultant = navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsultantDetailsPage');
  }

}
