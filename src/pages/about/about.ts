import { Component } from '@angular/core';
import { IonicPage, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';


@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  constructor(public http: Http, public storage: Storage, public loadingCtrl: LoadingController) { }
  aboutContent = '';

  ionViewDidLoad() {
    this.storage.get('userData')
      .then((userData) => {
        if (userData) {
          console.log(userData);
          let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
          headers.append('Authorization', 'Bearer ' + userData.access_token);
          let options = new RequestOptions({ headers: headers }); // Create a request option
          let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: '<div class="loader"></div>'
          });
          loading.present();
          this.http.get('http://viable.cbcloudapps.com/api/Content/AboutApp', options)
            .subscribe(res => {
              this.aboutContent = res.json();
              loading.dismiss();
            }, (err) => {
              console.error(err.code);
              loading.dismiss();
            });
        } else {
        }
      });
  }
}