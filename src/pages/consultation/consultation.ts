import { Component} from '@angular/core';
import { NavController ,AlertController,ModalController ,ToastController, LoadingController, Refresher} from 'ionic-angular';
 
import { Http, RequestOptions, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
 
@Component({
  selector: 'page-consultation',
  templateUrl: 'consultation.html',
})
export class ConsultationPage {

  pageIndex = 1;
  queryText = '';
  segment = 'new';
  consultations: any = [];
  stats = 1;
  isConsultant = false;
  loaded = false;

  constructor(
    public alertCtrl: AlertController,
    public http: Http,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController
) { }


ionViewDidLoad() {
  this.storage.get('userData')
      .then((userData) => {
          if (userData.TypeId == "3") {
              this.isConsultant = true;
          }
      });
  this.getConsultation(1, 0, null, null);
  // this.getConsultation(1, null);
}
trimDetails(details) {
    return details.substring(0, 55) + '...';
}
getConsultation(status: number, pageStart: number, infinitScroll: any, refresher: any) { //
  this.storage.get('userData')
      .then((userData) => {
          if (userData) {
              let headers = new Headers({ 'Content-Type': 'application/json' });
              headers.append('Authorization', 'Bearer ' + userData.access_token);
              let options = new RequestOptions({ headers: headers });

              let loading = this.loadingCtrl.create({
                  spinner: 'hide',
                  content: '<div class="loader"></div>'
              });
              let encodedUrl = 'http://viable.cbcloudapps.com/api/Consultations/getall?StatusId=' + status + '&pageStart=' + pageStart + '&pageSize=15';
              if (!refresher && !infinitScroll) {
                  loading.present();
              }
              this.http.get(encodedUrl, options)
                  .subscribe(res => {
                      let newConsultations = res.json();
                      console.log(newConsultations);
                      if (this.consultations.length && infinitScroll) {
                          for (let i = 0; i < newConsultations.length; i++) {
                              for (let k = 0; k < this.consultations.length; k++) {
                                  if (newConsultations[i].Id !== this.consultations[k]) {
                                      this.consultations.push(newConsultations[i]);
                                  }
                              }
                          }
                      } else {
                          this.consultations = res.json();
                      }
                      this.loaded = true;
                      loading.dismiss();
                      if (refresher) {
                          refresher.complete();
                      }
                      if (infinitScroll) {
                          infinitScroll.complete();
                      }
                  }, (err) => {
                      console.log(err);
                      this.loaded = true;
                      loading.dismiss();
                      this.consultations = [];
                      if (err.status == 401) {
                      } else if (err.status == 400) {
                          let alert = this.alertCtrl.create({
                              title: 'حدث خطأ ما ',
                              subTitle: 'نعتذر ولكن يبدو أن هناك خطأ ما ',
                              buttons: ['تم']
                          });
                          alert.present();
                      } else {
                          console.log(JSON.stringify(err));
                          let alert = this.alertCtrl.create({
                              title: 'حدث خطأ ما ',
                              subTitle: 'نعتذر ولكن يبدو أن هناك خطأ ما ',
                              buttons: ['تم']
                          });
                          alert.present();
                      }
                  });
          } else {
          }
      });
}

changeStatus() {
  this.loaded = false;
  this.consultations = [];
  switch (this.segment) {
      case 'new':
          this.stats = 1;
          break;
      case 'opened':
          this.stats = 2;
          break;
      case 'closed':
          this.stats = 3;
          break;
      default:
          this.stats = 1;
          break;
  }
  // this.getConsultation(this.stats, null);
  this.getConsultation(this.stats, 0, null, null);
}

presentAdd() {
  this.navCtrl.push('ConsultationAddPage');
}

goToConsultationDetail(consultationData: any) {
  this.storage.get('userData')
      .then((userData) => {
          if (userData) {
              let loading = this.loadingCtrl.create({
                  spinner: 'hide',
                  content: '<div class="loader"></div>'
              });
              loading.present();
              console.log(userData);
              let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
              headers.append('Authorization', 'Bearer ' + userData.access_token);
              let options = new RequestOptions({ headers: headers }); // Create a request option
              this.http.get('http://viable.cbcloudapps.com//api/Consultations/GetConsultation/' + consultationData.Id, options)
                  .subscribe(res => {
                      let data = res.json();
                      data.RepliesCount = consultationData.RepliesCount;
                      console.log(data);

                      switch (this.stats) {
                          case 1:
                              this.navCtrl.push('ConsultationDetailPage', data);
                              break;
                          case 2:
                              this.navCtrl.push('ConsultationChatPage', data);
                              break;
                          case 3:
                              this.stats = 3;
                              this.navCtrl.push('ConsultationDetailPage', data);
                              break;
                      }
                      loading.dismiss();
                  }, (err) => {
                      loading.dismiss();

                      console.log("Error ???!!" + JSON.stringify(err));
                  });
          }
      });
}
doRefresh(refresher: Refresher) {
  this.getConsultation(this.stats, 0, null, refresher);
}

doInfinite(infiniteScroll: any) {
  this.pageIndex += 15;
  // this.getConsultation(this.stats, infiniteScroll);
  this.getConsultation(this.stats, this.pageIndex, infiniteScroll, null);
}


}
