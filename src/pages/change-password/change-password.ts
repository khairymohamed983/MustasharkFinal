import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';
import { MyApp } from "../../app/app.component";

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  showPass1 = false;
  showPass2 = false;
  showPass3 = false;
  type1: string = 'password';
  type2: string = 'password';
  type3: string = 'password';

  model: any = {
    OldPassword: '',
    NewPassword: '',
    ConfirmPassword: ''
  }

  constructor(public events: Events, public app: MyApp, public navCtrl: NavController, public navParams: NavParams, public http: Http,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  showPassword(i: number) {
    switch (i) {
      case 1:
        this.showPass1 = !this.showPass1;
        if (this.showPass1) {
          this.type1 = 'text';
        } else {
          this.type1 = 'password';
        }
        break;
      case 2:
        this.showPass2 = !this.showPass2;
        if (this.showPass2) {
          this.type2 = 'text';
        } else {
          this.type2 = 'password';
        }
        break;
      case 3:
        this.showPass3 = !this.showPass3;
        if (this.showPass3) {
          this.type3 = 'text';
        } else {
          this.type3 = 'password';
        }
        break;
    }
  }

  changePassword(addForm: any) {
    if (addForm.valid) {
      this.storage.get('userData')
        .then((userData) => {
          if (userData) {
            console.log(userData);
            let headers = new Headers({ 'Content-Type': 'application/json' });  
            headers.append('Authorization', 'Bearer ' + userData.access_token);
            let options = new RequestOptions({ headers: headers });  
            let loading = this.loadingCtrl.create({
              spinner: 'hide',
              content: '<div class="loader"></div>'
            });
            loading.present();
            this.http.post('http://viable.cbcloudapps.com/api/Account/ChangePassword', this.model, options)
              .subscribe(res => {
                console.log(res);
                loading.dismiss();
                let alert = this.alertCtrl.create({
                  title: 'تم تغيير كلمة المرور بنجاح ',
                  subTitle: 'سيتوجب عليك الآن تسجيل الدخول  من جديد ',
                  buttons: ['تم']
                });
                alert.present();
                this.events.publish('user:logout');
              }, (err) => {
                loading.dismiss();
                let alert = this.alertCtrl.create({
                  title: 'عفوا لم نتمكن من تغيير كلمة المرور ',
                  subTitle: '  رجاءالتأكد من صحة كلمة المرور ثم أعد المحاولة ',
                  buttons: ['تم']
                });
                alert.present();
              });
          } else {
          }
        });
    } else {
      let alert = this.alertCtrl.create({
        title: 'الرجاء إكمال  البيانات ',
        subTitle: 'يبدو ان هناك حقل او اكثر لم يتم ملؤها  ',
        buttons: ['تم']
      });
      alert.present();

    }
  }

}
