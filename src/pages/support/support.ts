import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { IonicPage, AlertController, NavController, ToastController, LoadingController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';


@IonicPage()
@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
})
export class SupportPage {


  submitted: boolean = false;
  supportMessage: string;
  supportData: any = {
    Title: '',
    Email: '',
    Details: ''
  }
  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public http: Http,
    public loadingCtrl: LoadingController,
    public storage: Storage
  ) {

  }

  ionViewDidEnter() {
    this.storage.get('userData')
      .then((userData) => {
        if (userData) {
          this.supportData.Email = userData.Email;
        }
      });

  }

  submit(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.supportMessage = '';
      this.submitted = false;
      this.storage.get('userData')
        .then((userData) => {
          if (userData) {
            console.log(userData);
            let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
            headers.append('Authorization', 'Bearer ' + userData.access_token);
            let options = new RequestOptions({ headers: headers }); // Create a request option
            let loading = this.loadingCtrl.create({
              spinner: 'hide',
              content: '<div class="loader"></div>'
            });
            loading.present();
            this.http.post('http://viable.cbcloudapps.com/api/Complaints/Add', JSON.stringify(this.supportData), options)
              .subscribe(res => {
                console.log(res.json());
                loading.dismiss();
                form.reset();
                let toast = this.toastCtrl.create({
                  message: 'تم ارسال شكواك او استعلامك للادارة وسيتم الرد عليك قريبا الرجاء متابعة بريدك الالكتورني  ',
                  duration: 4000
                });
                toast.present();
                this.navCtrl.goToRoot({});
                // this.navCtrl.push('AccountPage');
              }, (err) => {
                console.log(err);
                loading.dismiss();
              });
          } else {
            //
          }
        });

    }
  }
  // If the user enters text in the support question and then navigates
  // without submitting first, ask if they meant to leave the page
  ionViewCanLeave(): boolean | Promise<boolean> {
    // If the support message is empty we should just navigate
    if (!this.supportMessage || this.supportMessage.trim().length === 0) {
      return true;
    }

    return new Promise((resolve: any, reject: any) => {
      let alert = this.alertCtrl.create({
        title: 'Leave this page?',
        message: 'Are you sure you want to leave this page? Your support message will not be submitted.'
      });
      alert.addButton({ text: 'Stay', handler: reject });
      alert.addButton({ text: 'Leave', role: 'cancel', handler: resolve });

      alert.present();
    });
  }
}
