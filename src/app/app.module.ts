import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, Slides } from 'ionic-angular';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Camera } from '@ionic-native/camera';
import { Stripe } from '@ionic-native/stripe';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {InAppPurchase} from '@ionic-native/in-app-purchase';

import { MyApp } from './app.component';
import { TabsPage } from "../pages/tabs/tabs";
import { HomePage } from "../pages/home/home";
import { AccountPage } from "../pages/account/account";
import { ConsultationPage } from "../pages/consultation/consultation";
import {PackagesPage} from '../pages/packages/packages';
import {ConsultantsPage} from '../pages/consultants/consultants';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    HomePage,
    AccountPage,
    ConsultationPage,
    PackagesPage,
    ConsultantsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'عودة',
      backButtonIcon: 'ios-arrow-back',
      iconMode: 'md'
    }),
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    HomePage,
    AccountPage,
    ConsultationPage,
    PackagesPage,
    ConsultantsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Camera,
    Stripe,
    Slides,
    InAppPurchase,
    Base64ToGallery
  ]
})
export class AppModule { }