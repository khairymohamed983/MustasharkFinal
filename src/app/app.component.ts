import { Component, ViewChild } from '@angular/core';
import { Events, MenuController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { TabsPage } from "../pages/tabs/tabs";
import { HomePage } from "../pages/home/home";

export interface PageInterface {
  title: string;
  name: string;
  // component: any;
  icon: string;
  logsOut?: boolean;
  index?: number;
  tabName?: string;
  // tabComponent?: any;
}
@Component({

  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  @ViewChild(Nav) nav: Nav;
  isMenuEnabled = false;
  menuePages: PageInterface[] = [
    // { title: 'الملف الشخصي', name: 'TabsPage', index: 0, icon: 'person' },
    { title: 'الإستشارات', name: 'TabsPage', index: 1, icon: 'chatboxes' },
    { title: 'الباقات', name: 'TabsPage', index: 2, icon: 'albums' },
    // { title: 'المستشارين', name: 'TabsPage', index: 3, icon: 'people' },
    { title: 'سجل الإشتراكات', name: 'PackageHistoryPage', icon: 'time' },
    { title: 'الشكاوي والدعم', name: 'SupportPage', icon: 'information-circle' },
    { title: 'عن مستشارك', name: 'AboutPage', icon: 'help' },
    { title: 'سياسة الإستخدام', name: 'PolicyPage', icon: 'pulse' },
    { title: 'تسجيل الخروج', name: 'LoginPage', icon: 'log-out', logsOut: true },
  ];
  outPages: PageInterface[] = [
    { title: 'تسجيل الدخول ', name: 'LoginPage', icon: 'pulse' },
    { title: 'إنشاء حساب', name: 'SignupPage', icon: 'pulse' },
    { title: 'سياسة الإستخدام', name: 'PolicyPage', icon: 'pulse' },
    { title: 'عن مستشارك', name: 'AboutPage', icon: 'help' }
  ];
  constructor(public events: Events, public menu: MenuController, public storage: Storage, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // statusBar.styleDefault();
      splashScreen.hide();
      platform.setDir('rtl', true);
      this.enableMenu(true);
      
      this.storage.get('userData')
        .then((userData) => {
          if (userData) {
            if (userData.TypeId == "3") {
              this.menuePages.splice(2, 1);
            } 
            this.enableMenu(true);
            this.rootPage = TabsPage;
          } else {
            this.enableMenu(false);
            this.storage.get('hasSeenTutorial')
              .then((hasSeenTutorial) => {
                if (hasSeenTutorial) {
                  this.rootPage = HomePage;
                } else {
                  this.rootPage = 'TutorialPage';
                }
              });
          }
        }).catch((err) => {
          this.enableMenu(false);
          this.rootPage = 'TutorialPage';
        });
    });
    this.listenToLoginEvents();
  }

  openPage(page: PageInterface) {
    if (page.logsOut === true) {
      this.events.publish('user:logout');
      return;
    }
    let params = { tabIndex: page.index || 0};
    if (this.nav.getActiveChildNavs().length && page.index != undefined) {
      this.nav.getActiveChildNavs()[0].select(page.index);
    } else {
      if (name == 'TabsPage') {
        this.nav.setRoot(TabsPage, params).catch((err: any) => {
          console.log(`Didn't set nav root: ${err}`);
        });
      } else {
        this.storage.set('next' , page.name).then(()=>{
          this.nav.setRoot(TabsPage, params).catch((err: any) => {
            console.log(`Didn't set nav root: ${err}`);
          });
        });
      }
    }
  }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn, 'loggedInMenu');
    this.menu.enable(!loggedIn, 'loggedOutMenu');
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.enableMenu(true);
      this.rootPage = TabsPage;
    });

    this.events.subscribe('user:signup', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:logout', () => {
      this.enableMenu(false);
      this.storage.set('userData', undefined).then(() => {
        this.rootPage = 'LoginPage';
      });
    });
  }

  gotoLogin() {
    this.nav.setRoot('LoginPage');
  }
}
